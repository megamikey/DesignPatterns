package Iterator_Pattern;

import java.util.Iterator;

public interface SongIterator {

    public Iterator createIterator();

}