package Command_Pattern;

public interface ElectronicDevice {

    public void on();

    public void off();

    public void volumeUp();

    public void volumenDown();

}